# quarkus-kubemq Project

This project uses Quarkus, the Supersonic Subatomic Java Framework as well as KubeMQ.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .
If you want to learn more about KubeMQ, please visit its website: https://kubemq.io/ .

## Prerequisite

Beforehand, you have to run KubeMQ container locally as follows :

```shell script
docker run --rm -d -p 8080:8080 -p 50001:50000 -p 9090:9090  -v /<project_path>/quarkus-kubemq/config.yaml:/kubemq/config.yaml kubemq/kubemq-community:v2.3.7
```

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8081/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

## Related Guides

- AMQP 1.0 JMS client - Apache Qpid JMS ([guide](https://quarkus.io/guides/jms)): Use JMS APIs with AMQP 1.0 servers such as ActiveMQ Artemis, ActiveMQ 5, Qpid Broker-J, Qpid Dispatch router, Azure Service Bus, and more
- YAML Configuration ([guide](https://quarkus.io/guides/config#yaml)): Use YAML to configure your Quarkus application

### YAML Config

Configure your application with YAML

[Related guide section...](https://quarkus.io/guides/config-reference#configuration-examples)

The Quarkus application configuration is located in `src/main/resources/application.yaml`.

### RESTEasy Reactive

In order to send a message into kubeMQ queue

curl -X POST http://localhost:8081/kube-mq -H "Content-Type: application/json" -d '{"message": "hello world", "metaData": "metadata is here"}'

In order to receive a couple of messages into kubeMQ queue

curl -X GET http://localhost:8081/kube-mq -H "Content-Type: application/json"
