package org.hatami.api;

import io.kubemq.sdk.queue.ReceiveMessagesResponse;
import io.kubemq.sdk.queue.SendMessageResult;
import org.hatami.api.model.MessageModel;
import org.hatami.messaging.KubeManager;
import org.hatami.persistence.MicroStreamManager;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/kube-mq")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class KubeMQResource {

    @Inject
    KubeManager kubeManager;
    @Inject
    MicroStreamManager microStreamManager;

    @POST
    public Response sendMessage(final MessageModel message) {

        // store an object into storage
        microStreamManager.getStorageManager().store(message);

        SendMessageResult resSend = kubeManager
                .sendMessage(message.getMessage(), message.getMetaData());

        if (resSend.getIsError()) {
            return Response.status(409).entity(resSend.getError()).build();
        }
        return Response.ok(resSend.getMessageID()).build();
    }

    @GET
    public Response receiveMessage() {
        ReceiveMessagesResponse resRec = kubeManager.ReceiveMessage(10, 1);

        if (resRec.getIsError()) {
            return Response.status(409).entity(resRec.getError()).build();
        }

        return Response.ok(kubeManager.getMessageBody(resRec)).build();
    }
}