package org.hatami.api.model;

public class MessageModel {
    private String message;
    private String metaData;

    public MessageModel() {
    }

    public MessageModel(String message, String metaData) {
        this.message = message;
        this.metaData = metaData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }
}
