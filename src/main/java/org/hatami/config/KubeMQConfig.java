package org.hatami.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithName;

@ConfigMapping(prefix = "kube-mq")
public interface KubeMQConfig {

    @WithName("server-address")
    String serverAddress();

    @WithName("client-id")
    String clientId();
    @WithName("queue-name")
    String queueName();
}