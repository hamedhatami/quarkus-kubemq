package org.hatami.messaging;


import io.kubemq.sdk.basic.ServerAddressNotSuppliedException;
import io.kubemq.sdk.queue.Message;
import io.kubemq.sdk.queue.Queue;
import io.kubemq.sdk.queue.ReceiveMessagesResponse;
import io.kubemq.sdk.queue.SendMessageResult;
import io.kubemq.sdk.tools.Converter;
import org.hatami.config.KubeMQConfig;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.net.ssl.SSLException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@ApplicationScoped
public class KubeManager {

    @Inject
    KubeMQConfig kubeMQConfig;

    private static Queue queue = null;

    public SendMessageResult sendMessage(final String message,
                                         final String metaData) {
        try {
            return getQueue()
                    .SendQueueMessage(new Message()
                            .setBody(Converter.ToByteArray(message))
                            .setMetadata(metaData));
        } catch (ServerAddressNotSuppliedException | IOException e) {
            return null;
        }
    }

    public ReceiveMessagesResponse ReceiveMessage(final int messageNumber,
                                                  final int waitingTime) {
        try {
            return getQueue()
                    .ReceiveQueueMessages(messageNumber, waitingTime);
        } catch (SSLException | ServerAddressNotSuppliedException e) {
            return null;
        }
    }

    public List<String> getMessageBody(final ReceiveMessagesResponse resRec) {
        List<String> result = new ArrayList<>();
        resRec.getMessages().forEach(msg -> {
            try {
                result.add(String.valueOf(Converter.FromByteArray(msg.getBody())));
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        });

        return result;
    }

    public Queue getQueue() throws ServerAddressNotSuppliedException, SSLException {

        if (Objects.isNull(queue)) {
            queue = new Queue(kubeMQConfig.queueName(),
                    kubeMQConfig.clientId(),
                    kubeMQConfig.serverAddress());
        }

        return queue;
    }
}
