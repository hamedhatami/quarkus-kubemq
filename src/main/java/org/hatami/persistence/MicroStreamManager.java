package org.hatami.persistence;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import one.microstream.storage.embedded.types.EmbeddedStorage;
import one.microstream.storage.embedded.types.EmbeddedStorageManager;
import org.hatami.persistence.model.DataRoot;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import java.nio.file.Paths;
import java.util.Date;

@ApplicationScoped
public class MicroStreamManager {
    private EmbeddedStorageManager storageManager;

    void onStart(@Observes StartupEvent ev) {
        final DataRoot root = new DataRoot();
        storageManager = EmbeddedStorage.start(
                root,             // root object
                Paths.get("data") // storage directory
        );

        root.setContent("Hello World! @ " + new Date());
        storageManager.storeRoot();

    }

    void onStop(@Observes ShutdownEvent ev) {
        storageManager.shutdown();
    }

    public EmbeddedStorageManager getStorageManager() {
        return storageManager;
    }
}
