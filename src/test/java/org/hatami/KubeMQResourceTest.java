package org.hatami;

import io.quarkus.test.junit.QuarkusTest;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasItem;

@QuarkusTest
public class KubeMQResourceTest {

    @Test
    public void testKubeMQPostEndpoint() {
        given()
                .body("{\"message\": \"hello world\", \"metaData\": \"metadata is hre\"}")
                .contentType("application/json")
                .when()
                .post("/kube-mq")
                .then()
                .statusCode(200);
    }

    @Test
    public void testKubeMQGetEndpoint() {
        given()
                .contentType("application/json")
                .when()
                .get("/kube-mq")
                .then()
                .statusCode(200)
                .body("$", Matchers.hasItem(("hello world")));
    }

}